class Potepan::CategoriesController < ApplicationController
  def show
    @taxons = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @taxons_product = @taxons.products.includes(master: :default_price)
  end
end
