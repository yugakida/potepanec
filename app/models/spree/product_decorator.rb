Spree::Product.class_eval do
  DISPLAY_MAX_RELATED_PRODUCT = 4

  scope :includes_images_and_price, -> { includes(master: %i[images default_price]) }
  scope :except_product, ->(product) { where.not(id: product.id) }

  def related_products
    self.class
        .joins(:taxons)
        .includes_images_and_price
        .where(spree_taxons: { id: taxons.ids })
        .except_product(self)
        .distinct
        .sample(DISPLAY_MAX_RELATED_PRODUCT)
  end
end
