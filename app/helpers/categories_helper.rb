module CategoriesHelper
  def grid_params?
    params[:layout] == 'grid'
  end
end
