require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do

  describe "#show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { taxonomy.root }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_properties){ create(:product_property, product: product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns @product_properties" do
      expect(assigns(:product_properties)).to eq product.product_properties
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end