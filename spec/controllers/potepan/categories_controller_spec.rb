require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { taxonomy.root }
  let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    describe "right value in instance variable" do 
      it "has right value in @taxons" do
        expect(assigns(:taxons)).to eq taxon
      end
  
      it "has right value in @taxonomies" do
        expect(assigns(:taxonomies)).to match_array taxonomy
      end
  
      it "has right value in @taxons_product" do
        expect(assigns(:taxons_product)).to eq taxon.products
      end
    end

    it "render the :show template" do
      expect(response).to render_template :show
    end
  end
end
