require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:categories_taxonomy){ create(:taxonomy, name: "categories") }
  let!(:brand_taxonomy){ create(:taxonomy, name: "brand")}

  let!(:bag_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "bag_taxon") }
  let!(:mug_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "mug_taxon") }
  let!(:shirt_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "shirt_taxon") }
  let!(:rails_taxon){ create(:taxon, taxonomy: brand_taxonomy, parent: brand_taxonomy.root, name: "rails_taxon") }
  let!(:apache_taxon){ create(:taxon, taxonomy: brand_taxonomy, parent: brand_taxonomy.root, name: "apache_taxon")}

  let!(:rails_bag_product){ create(:product, taxons: [bag_taxon, rails_taxon], name: "rails_bag_product", price: 10)}
  let!(:rails_mug_product){ create(:product, taxons: [mug_taxon, rails_taxon], name: "rails_mug_product", price: 20)}
  let!(:apache_shirt_product){ create(:product, taxons: [shirt_taxon, apache_taxon], name: "apache_shirt_product", price: 30)}
  let!(:related_product_list){ create_list(:product, 8, taxons: [shirt_taxon], name: "related_product_list" ) }

  let!(:DISPLAY_MAX_RELATED_PRODUCT){ 4 }


  describe "related_products have correct value, but don't have self product" do
    it "has correct value" do
      expect(rails_bag_product.related_products).to eq [rails_mug_product]
      expect(rails_bag_product.related_products).to_not eq [apache_shirt_product]
    end

    it "doesn't have self product" do
      expect(rails_bag_product.related_products).to_not eq [rails_bag_product]
    end
  end

  it "has up to 4 related products" do
    expect(Spree::Product.find_by(name: "apache_shirt_product").related_products.size).to eq DISPLAY_MAX_RELATED_PRODUCT
  end
end