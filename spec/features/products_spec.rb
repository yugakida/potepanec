require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:categories_taxonomy){ create(:taxonomy, name: "categories") }
  given!(:brand_taxonomy){ create(:taxonomy, name: "brand")}

  given!(:bag_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "bag_taxon") }
  given!(:mug_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "mug_taxon") }
  given!(:shirt_taxon){ create(:taxon, taxonomy: categories_taxonomy, parent: categories_taxonomy.root, name: "shirt_taxon") }
  given!(:rails_taxon){ create(:taxon, taxonomy: brand_taxonomy, parent: brand_taxonomy.root, name: "rails_taxon") }
  given!(:apache_taxon){ create(:taxon, taxonomy: brand_taxonomy, parent: brand_taxonomy.root, name: "apache_taxon")}

  given!(:rails_bag_product){ create(:product, taxons: [bag_taxon, rails_taxon], name: "rails_bag_product", price: 10)}
  given!(:rails_mug_product){ create(:product, taxons: [mug_taxon, rails_taxon], name: "rails_mug_product", price: 20)}
  given!(:apache_shirt_product){ create(:product, taxons: [shirt_taxon, apache_taxon], name: "apache_shirt_product", price: 30)}

  before do
    visit potepan_product_path(rails_bag_product.id)
  end

  feature "User can look at correct products information" do
    scenario "display product name and price" do
      expect(page).to have_selector ".rightside_product_name", text: rails_bag_product.name
      expect(page).to have_selector ".rightside_product_price", text: rails_bag_product.display_price

      within '.page-title' do
        expect(page).to have_content rails_bag_product.name
      end
    end
  
    scenario "display related product name and price, but don't have self product" do
      within '.productCaption' do
        expect(page).to have_content rails_mug_product.name
        expect(page).to have_content rails_mug_product.display_price
        expect(page).to_not have_content apache_shirt_product.name
        expect(page).to_not have_content apache_shirt_product.display_price
      end
    end
  end
 
  feature "click and jump correct page" do
    scenario "click on the related product name" do
      click_link rails_mug_product.name
      expect(current_path).to eq potepan_product_path(rails_mug_product.id)
    end

    scenario "click on the related product price" do
      click_link rails_mug_product.display_price
      expect(current_path).to eq potepan_product_path(rails_mug_product.id)
    end

    scenario "click on 一覧ページへ戻る"do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(rails_bag_product.taxons.first.id)
   end
  end

  scenario "display correct title" do
    expect(page).to have_title rails_bag_product.name + ' | potepanec'
  end
end
