require 'rails_helper'

RSpec.feature "Categories", type: :feature do
given!(:taxonomy) { create(:taxonomy) }
given!(:taxon) { taxonomy.root }
given!(:product) { create(:product, taxons: [taxon]) }
given!(:taxon_child) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }

  before do
    visit potepan_category_path(taxon.id)
  end

  feature "this page is correct" do
    scenario "display correct name" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content product.name
    end
  
    scenario "have correct link" do
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
      expect(page).to have_link taxon_child.name, href: potepan_category_path(taxon_child.id)
    end
  end

  feature "click something and jump correct page" do
    scenario "click taxon by category_brand_panel_taxon" do
      within '.category_brand_panel_taxon' do
        click_link taxon_child.name
        expect(current_path).to eq potepan_category_path(taxon_child.id)
      end
    end
  
    scenario "click taxon by search_in_all_taxon" do
      within '.search_in_all_taxon' do
        click_link taxon_child.name
        expect(current_path).to eq potepan_category_path(taxon_child.id)
      end
    end
  
    scenario "click product" do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end